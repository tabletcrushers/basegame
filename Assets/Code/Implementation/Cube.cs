﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Cube : MonoBehaviour
{
    StateMachine stateMachine;
    FinalObjectPoolsCollection<string, GameObject> op;

    int cnt = 0;
    GameObject to;

    void Start()
    {
        KeyValuePair<GameObject, List<GameObject>> t = new KeyValuePair<GameObject, List<GameObject>>(to, new List<GameObject>());


        // Debug.Log(t.Key == null);

        PluginsQueue testQueue = new PluginsQueue();
        testQueue.AddPlugin(new CubeFetureRotate(2));

        TestPredicate p = new TestPredicate();

        ExitFromStateTransition ttt = new ExitFromStateTransition("Test1", new IEvaluable[] { p });

        State s = new State("Test", testQueue, new IExitFromStateTransition[] { ttt });


        PluginsHeap testHeap = new PluginsHeap();
        testHeap.AddPlugin(new TestPlugin("FINISH"));

        PluginsQueue testQueue1 = new PluginsQueue();
        testQueue1.AddPlugin(new TestPlugin("FINISH"));

        State s1 = new State("Test1", testHeap, new IExitFromStateTransition[0]);

        stateMachine = new StateMachine(new State[] { s, s1 });


        op = new FinalObjectPoolsCollection<string, GameObject>();

        Action<GameObject> a;
        a = Ta;
        a += Ta1;

        op.AddPool("Test", (GameObject go) => go.transform.position.x > 14, a);

        GameObject tObj = Instantiate(Resources.Load<GameObject>("Level"));
        op.AddObject("Test", tObj);

        TestGet();
    }

    void HandleAction(GameObject obj)
    {
    }


    void Ta(GameObject o)
    {
        o.transform.localScale = new Vector3(2, 2, 1);
    }
    void Ta1(GameObject o)
    {

    }

    void Update()
    {
        //stateMachine.Update(Time.deltaTime);
        op.Update(Time.deltaTime);

        to.transform.Translate(new Vector3(.4f, 0, 0));

        float height = 2f * Camera.main.orthographicSize;
        float width = height * Camera.main.aspect;

        // Debug.Log(to.transform.position.x > width);

        cnt++;
        if (cnt == 70) TestGet();
        if (cnt == 140) TestGet();
        if (cnt == 210) TestGet();
        if (cnt == 280) TestGet();
        if (cnt == 350) TestGet();
        if (cnt == 420) TestGet();
        if (cnt == 490) TestGet();
        if (cnt == 560) TestGet();
        if (cnt == 630) TestGet();
    }

    void TestGet()
    {
        to = op.GetObject("Test");
        to.transform.position = new Vector3(0, 0, 0);
    }
}

class TestNewPredicate : AbstractPredicate
{
    override public bool Evaluate(object obj = null)
    {
        return (obj as GameObject).transform.position.x > 14;
    }
}

class TestPredicateOP : AbstractPredicate
{
    int count;

    override public bool Evaluate(object obj = null)
    {
        count++;
        return count > 60;
    }

    public override void Reset()
    {
        count = 0;
    }
}

class TestPredicate : IEvaluable
{
    readonly GameObject bg;

    public TestPredicate()
    {
        bg = GameObject.Find("bg_1");
    }

    public bool Evaluate(object go)
    {
        return bg.transform.position.x > 1;
    }
}

class TestPlugin : AbstractPlugin
{
    string msg;

    public TestPlugin(string m)
    {
        msg = m;
    }
    public override void Start()
    {
        Stop();
    }
}

class TestPlugin1 : AbstractPlugin
{
    int delay = 210;

    public override void Update(float delta)
    {
        delay--;

        if (delay < 0)
        {
            Stop();
        }
    }
}
