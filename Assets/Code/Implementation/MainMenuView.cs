﻿using UnityEngine;

public class MainMenuView : MonoBehaviour, IView
{
    public bool IsActive { get; set; }

    public GameObject Lb;
    public GameObject Rb;

    readonly float LbHidenXPosition = -180;
    readonly float LbVisbleXPosition = 20;

    readonly float RbHidenXPosition = -160;
    readonly float RbVisbleXPosition = -20;

    void Start()
    {
        GameObject.Find("GUI/Z").transform.position = new Vector3(0, 0, 0);
    }

    public void Show()
    {
        
    }

    public void Hide()
    {
        TweenSharp tween = new TweenSharp(Lb.GetComponent<RectTransform>(), .7f, new
        {
            x = LbHidenXPosition,
            // y = mm.transform.position.y - 1f,
            //alpha = 0f,
            ease = Elastic.EaseOut,
            onComplete = new System.Action(OnCompleteHide)
        });
    }

    public void OnLbClick()
    {
        Hide();
    }

    private void OnCompleteHide()
    {
        gameObject.SetActive(false);
    }
}
