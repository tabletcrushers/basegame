﻿using UnityEngine;

public class CubeFetureRotate : AbstractPlugin, IObserver
{
    GameObject bg;
    IObservable obs;
    readonly int lim;

    public CubeFetureRotate(int lim = 1)
    {
        bg = GameObject.Find("bg_1");
        obs = new Observable();
        this.lim = lim;
    }

    public void ProcessNotification(params object[] context)
    {
        UnityEngine.Debug.Log("OOO__EVENT!!" + context[1]);
    }

    public override void Update(float delta)
    {
        if (LifeState == LifeCycleState.Stopped)
        {
            return;
        }

        bg.gameObject.transform.Translate(new Vector3(.05f, 0, .01f));
        if (bg.gameObject.transform.position.x > lim)
        {
            obs.Notify("Test", 1, "string", .1f);
            obs.RemoveObserver("Test", this);

            Stop();
        }
    }
}
