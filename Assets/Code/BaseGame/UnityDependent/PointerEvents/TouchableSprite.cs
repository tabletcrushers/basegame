﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TouchableSprite : MonoBehaviour, IPointerDownHandler
{
    void Start()
    {
        AddPhysics2DRaycaster();
    }

    void AddPhysics2DRaycaster()
    {
        Physics2DRaycaster physicsRaycaster = FindObjectOfType<Physics2DRaycaster>();

        if (physicsRaycaster == null)
        {
            Camera.main.gameObject.AddComponent<Physics2DRaycaster>();
        }
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Clicked");
    }
}
