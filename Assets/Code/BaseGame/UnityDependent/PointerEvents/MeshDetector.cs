﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MeshDetector : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    void Start()
    {
        addPhysicsRaycaster();
        Debug.Log(FindObjectOfType<PhysicsRaycaster>());
    }

    void addPhysicsRaycaster()
    {
        Physics2DRaycaster physicsRaycaster = FindObjectOfType<Physics2DRaycaster>();
        if (physicsRaycaster == null)
        {
            Camera.main.gameObject.AddComponent<Physics2DRaycaster>();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Clicked: " /*+ eventData.pointerCurrentRaycast.gameObject.name*/);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Debug.Log("UPPP: ");
    }

    //Implement Other Events from Method 1
}