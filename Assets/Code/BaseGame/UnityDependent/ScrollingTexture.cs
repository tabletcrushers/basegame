﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingTexture : MonoBehaviour
{
    public Vector2 Scroll = new Vector2(-0.5f, 0);
    Vector2 Offset = new Vector2(0f, 0f);
    SpriteRenderer renderer;

    //private Material _material;

    void Start()
    {
        renderer = GetComponent<SpriteRenderer>();

        //_material = GetComponent<Renderer>().material;
    }

    void Update()
    {
        // Debug.Log(GetComponent<Renderer>().material);
        Offset += Scroll * Time.deltaTime;
        renderer.material.SetTextureOffset("_MainTex", Offset);

        //_material.mainTextureOffset = new Vector2(Time.time * .5f, 0);
    }
}
