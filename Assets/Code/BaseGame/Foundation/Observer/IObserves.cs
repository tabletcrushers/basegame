﻿public interface IObservable
{
    void AddObserver(string expectedEventName, IObserver observer);
    void RemoveObserver(string expectedEventName, IObserver observer);
    void Notify(string eventName, params object[] context);
    void Clear();
}

public interface IObserver
{
    void ProcessNotification(params object[] context);
}