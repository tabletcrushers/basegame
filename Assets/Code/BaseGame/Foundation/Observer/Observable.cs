﻿using System.Collections.Generic;

public class Observable : IObservable
{
    Dictionary<string, List<IObserver>> observersMap;

    public void Clear()
    {
        observersMap = new Dictionary<string, List<IObserver>>();
    }

    public void AddObserver(string expectedEventName, IObserver observer)
    {
        if (observersMap == null)
        {
            Clear();
        }

        if (!observersMap.TryGetValue(expectedEventName, out List<IObserver> observers))
        {
            observers = new List<IObserver>();
            observersMap.Add(expectedEventName, observers);
        }

        observers.Add(observer);
    }

    public void RemoveObserver(string expectedEventName, IObserver observer)
    {
        if (observersMap == null)
        {
            return;
        }

        if (!observersMap.TryGetValue(expectedEventName, out List<IObserver> observers))
        {
            return;
        }

        observers.Remove(observer);
    }

    public void Notify(string expectedEventName, params object[] context)
    {
        if (observersMap == null)
        {
            return;
        }

        if (!observersMap.TryGetValue(expectedEventName, out List<IObserver> observers))
        {
            return;
        }

        for (int i = observers.Count - 1; i >= 0; i--)
        {
            if (observers[i] == null)
            {
                observers.Remove(observers[i]);
                break;
            }

            observers[i].ProcessNotification(context);
        }
    }
}