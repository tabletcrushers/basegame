﻿public abstract class AbstractPluginsContainer : AbstractPlugin, IPluginsContainer
{
    public virtual IPluginsContainer AddPlugin(IPlugin plugin)
    {
        plugins.Add(plugin);

        return (IPluginsContainer)this;
    }

    public virtual IPluginsContainer RemovePlugin(IPlugin plugin)
    {
        plugins.Remove(plugin);

        return (IPluginsContainer)this;
    }
}
