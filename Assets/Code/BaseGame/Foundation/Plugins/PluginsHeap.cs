﻿public class PluginsHeap : AbstractPluginsContainer
{
    public override void Update(float delta)
    {
        if (LifeState == LifeCycleState.Stopped)
        {
            return;
        }

        LifeState = LifeCycleState.Stopped;

        foreach (IPlugin plugin in plugins)
        {
            if (plugin.LifeState == LifeCycleState.Stopped)
            {
                if (plugin.Responsibility == PluginResponsibilities.Parent)
                {
                    Stop();

                    return;
                }

                continue;
            }

            LifeState = LifeCycleState.Started;

            plugin.Update(delta);
        }
    }

    public override void Start()
    {
        base.Start();

        foreach (IPlugin plugin in plugins)
        {
            plugin.Start();
        }
    }

    public override void Stop()
    {
        base.Stop();

        foreach (IPlugin plugin in plugins)
        {
            plugin.Stop();
        }
    }

}
