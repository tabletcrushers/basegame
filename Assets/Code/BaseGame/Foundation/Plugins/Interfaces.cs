﻿public interface IPlugin : IUpdatable, ILifeCycle
{ 
    PluginResponsibilities Responsibility { get; set; }
}

public interface IPluginsContainer : IPlugin
{
    IPluginsContainer AddPlugin(IPlugin plugin);
    IPluginsContainer RemovePlugin(IPlugin plugin);
}
