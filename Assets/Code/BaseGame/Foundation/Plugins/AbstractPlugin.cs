﻿using System.Collections.Generic;

public abstract class AbstractPlugin: IPlugin
{
    public PluginResponsibilities Responsibility { get; set; }
    public LifeCycleState LifeState { get; set; }

    readonly protected List<IPlugin> plugins;

    protected AbstractPlugin()
    {
        Responsibility = PluginResponsibilities.Regular;
        LifeState = LifeCycleState.Stopped;
        plugins = new List<IPlugin>();
    }

    public virtual void Start()
    {
        LifeState = LifeCycleState.Started;
    }

    public virtual void Stop()
    {
        LifeState = LifeCycleState.Stopped;
    }

    public virtual void Update(float delta)
    {

    }
}
