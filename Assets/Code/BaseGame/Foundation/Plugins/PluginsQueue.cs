﻿public class PluginsQueue : AbstractPluginsContainer
{
    private IPlugin currentPlugin;

    public override void Update(float delta)
    {
        if (LifeState == LifeCycleState.Stopped)
        {
            return;
        }

        if (currentPlugin.LifeState == LifeCycleState.Stopped)
        {
            if (currentPlugin.Responsibility == PluginResponsibilities.Parent)
            {
                Stop();

                return;
            }

            int nextIndex = plugins.IndexOf(currentPlugin) + 1;

            if (nextIndex >= plugins.Count)
            {
                Stop();

                return;
            }

            currentPlugin = plugins[nextIndex];
            currentPlugin.Start();
        }

        currentPlugin.Update(delta);
    }

    public override void Start()
    {
        base.Start();
        currentPlugin = plugins[0];
        currentPlugin.Start();
    }

    public override void Stop()
    {
        base.Stop();
        currentPlugin.Stop();
        currentPlugin = null;
    }
}
