﻿using System;
using System.Linq;
using System.Collections.Generic;

public interface IObjectPool<T>
{
    void Add(T obj);
    T Get();
}

public interface IObjectPoolsCollection<K, V>
{
    void AddObject(K poolID, V obj);
    V GetObject(K poolID);
}

public abstract class AbstractBaseObjectPool<T> : IObjectPool<T>
{
    protected List<T> _availableObjects;
    protected List<T> _inUseObjects;
    protected Action<T> _resetActions;

    protected AbstractBaseObjectPool(Action<T> resetActions = null)
    {
        _availableObjects = new List<T>();
        _inUseObjects = new List<T>();
        _resetActions = resetActions;
    }

    public void Add(T obj)
    {
        _availableObjects.Add(obj);
    }

    public T Get()
    {
        if (_availableObjects.Count == 0)
        {
            return default;
        }

        T instance = _availableObjects[0];
        _inUseObjects.Add(instance);
        _availableObjects.Remove(instance);

        return instance;
    }
}

public class ManualBackObjectPool<T> : AbstractBaseObjectPool<T>
{
    public ManualBackObjectPool(Action<T> resetActions = null) : base(resetActions) { }

    public void PutObjectBack(T obj)
    {
        if (!_inUseObjects.Remove(obj))
        {
            throw new System.Exception($"{obj} Object is not using");
        }

        _resetActions?.Invoke(obj);
        _availableObjects.Add(obj);
    }
}

public class AutoBackObjectPool<T> : AbstractBaseObjectPool<T>
{
    private readonly Predicate<T> _backPredicates;

    public AutoBackObjectPool(Predicate<T> backToPoolPredicates, Action<T> resetActions = null) : base(resetActions)
    {
        _backPredicates = backToPoolPredicates;
    }

    public void TryPutObjectsBack()
    {
        List<T> readyToBack = _inUseObjects.FindAll(_backPredicates);

        if (readyToBack.Count == 0)
        {
            return;
        }

        if (_resetActions != null)
        {
            foreach(T obj in readyToBack)
            {
                _resetActions(obj);
            }
        }

        _availableObjects.AddRange(readyToBack);
        _inUseObjects = _inUseObjects.Except(readyToBack).ToList();
    }
}

public abstract class AbstractObjectPoolsCollection<K, V> : IObjectPoolsCollection<K, V>
{
    protected Dictionary<K, IObjectPool<V>> _pools = new Dictionary<K, IObjectPool<V>>();

    public virtual void AddObject(K poolID, V obj)
    {
        if (!_pools.TryGetValue(poolID, out IObjectPool<V> pool))
        {
            PoolIsNotAddedHandler(poolID);
        }

        pool.Add(obj);
    }

    public virtual V GetObject(K poolID)
    {
        if (!_pools.TryGetValue(poolID, out IObjectPool<V> pool))
        {
            PoolIsNotAddedHandler(poolID);
        }

        V obj = pool.Get();

        if(obj == default)
        {
            PoolIsEmptyHandler(poolID);
        }

        return obj;
    }

    protected virtual void PoolIsNotAddedHandler(K poolID)
    {
        throw new Exception($"You haven't added pool {poolID}");
    }

    protected virtual void PoolIsEmptyHandler(K poolID)
    {
        throw new Exception($"Pool {poolID} is empty");
    }

    protected virtual void PoolExistsHandler(K poolID)
    {
        throw new Exception($"Pool {poolID} already exists");
    }
}

public class ManualObjectPoolsCollection<K, V> : AbstractObjectPoolsCollection<K, V>
{
    public virtual void AddPool(K poolID, Action<V> resetAction = null)
    {
        if (!_pools.TryGetValue(poolID, out IObjectPool<V> pool))
        {
            ManualBackObjectPool<V> poolToAdd = new ManualBackObjectPool<V>(resetAction);

              _pools.Add(poolID, poolToAdd);

            return;
        }

        PoolExistsHandler(poolID);
    }

    public void PutObjectBack(K poolID, V obj)
    {
        if (!_pools.TryGetValue(poolID, out IObjectPool<V> pool))
        {
            throw new Exception($"There is no pool {poolID}");
        }

        (pool as ManualBackObjectPool<V>).PutObjectBack(obj);
    }
}

public class FinalObjectPoolsCollection<K, V> : AbstractObjectPoolsCollection<K, V>, IUpdatable
{
    public virtual void AddPool(K poolID, Predicate<V> backToPoolPredicates, Action<V> resetActions = null)
    {
        if (!_pools.TryGetValue(poolID, out IObjectPool<V> pool))
        {
            AutoBackObjectPool<V> poolToAdd = new AutoBackObjectPool<V>(backToPoolPredicates, resetActions);

            _pools.Add(poolID, poolToAdd);

            return;
        }

        PoolExistsHandler(poolID);
    }

    public void Update(float delta)
    {
        foreach (KeyValuePair<K, IObjectPool<V>> pool in _pools)
        {
            (pool.Value as AutoBackObjectPool<V>).TryPutObjectsBack();
        }
    }
}
