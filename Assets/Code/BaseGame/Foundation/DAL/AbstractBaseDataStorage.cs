﻿using System.Collections.Generic;

public abstract class KeyedStacks<K, T> : Dictionary<K, List<T>>
{
    public void AddItem(K key, T item)
    {
        if (!TryGetValue(key, out List<T> objectsList))
        {
            objectsList = new List<T>();
        }

        objectsList.Add(item);
    }

    public T GetItem(K key)
    {
        if (TryGetValue(key, out List<T> objectsList))
        {
            throw new System.Exception("44");
        }

        T item = objectsList[0];
        objectsList.Remove(item);

        return item;
    }
}
