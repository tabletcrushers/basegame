﻿public interface IView
{
    bool IsActive { get; set; }
    void Show();
    void Hide();
}