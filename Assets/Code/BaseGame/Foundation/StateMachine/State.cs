﻿public class State : IState
{
    public string Name { get; }
    public LifeCycleState LifeState { get; set; }

    private IPlugin _features;
    private readonly IExitFromStateTransition[] exitTransitions;

    public State(string name, IPlugin features, IExitFromStateTransition[] exitTransitions)
    {
        Name = name;
        this.exitTransitions = exitTransitions;
        this._features = features;
    }

    public void Update(float delta)
    {
        if (LifeState == LifeCycleState.Started) _features.Update(delta);
    }

    public void Start()
    {
        _features.Start();
        LifeState = LifeCycleState.Started;
    }

    public void Stop()
    {
        _features.Stop();
        LifeState = LifeCycleState.Stopped;
    }

    public bool TryToGetExitTransition(out IExitFromStateTransition transition)
    {
        foreach (IExitFromStateTransition t in exitTransitions)
        {
            if (t.Evaluate())
            {
                transition = t;

                return true;
            }
        }

        transition = null;

        return false;
    }
}
