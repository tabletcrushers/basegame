﻿

public class StateMachine : IUpdatable
{
    private IState _currentState;
    private readonly IState[] _states;

    public StateMachine(IState[] states)
    {
        this._states = states;
        SetStateByName(states[0].Name);
    }

    public void Update(float delta)
    {
        _currentState.Update(delta);

        TryToSwitchState();
    }

    private void TryToSwitchState()
    {
        if(_currentState.TryToGetExitTransition(out IExitFromStateTransition exitTransition))
        {
            _currentState.Stop();
            SetStateByName(exitTransition.DestinationStateName);
        }
    }

    private void SetStateByName(string stateName)
    {
        foreach (IState state in _states)
        {
            if (state.Name == stateName)
            {
                _currentState = state;
                _currentState.Start();

                return;
            }
        }
    }
}
